//: Playground - noun: a place where people can play

import UIKit



protocol BaseSectionManagerViewModel: NSObjectProtocol
{}

protocol BaseSectionManagerProtocol: NSObjectProtocol, UITableViewDataSource, UITableViewDelegate
{
    init?(viewModel: BaseSectionManagerViewModel)
}


extension BaseSectionManagerProtocol
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60 // Consts.heightConstants.basicRow
    }
}

protocol CommandSectionManagerViewModel: BaseSectionManagerViewModel
{
    func commandMethod()
}

class CommandSectionManager: NSObject, BaseSectionManagerProtocol
{
    weak var viewModel: CommandSectionManagerViewModel!
    
    required init?(viewModel: BaseSectionManagerViewModel)
    {
        if let model = viewModel as? CommandSectionManagerViewModel
        {
            self.viewModel = model
        }
        else
        {
            return nil
        }
    }
}


class ViewModel: NSObject, CommandSectionManagerViewModel
{
    var sectionManagers = [BaseSectionManagerProtocol]()
    
    func scenePressed()
    {
        print("child name function is called!")
    }
    
    
    func setup<T: BaseSectionManagerProtocol>(with types: [T.Type])
    {
        for type in types
        {
            if let sectionManager = type.init(viewModel: self)
            {
                self.sectionManagers.append(sectionManager)
            }
        }
    }
    
    func commandMethod()
    {
        
    }
}

let viewModel = ViewModel()
viewModel.setup(with: [CommandSectionManager.self])
print(viewModel.sectionManagers)






